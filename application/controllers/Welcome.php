<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}
	public function ajax_manage_page()
  	{
      	$getData = $this->DemoModel->get_datatables();
    
      	$data = array();   

      	foreach ($getData as $Data) 
      	{   
			$base = base64_encode($Data->id);
			//print_r($base);exit;
			$btn = anchor(site_url('Banners/update/'.$base),'<button title="Edit" class="btn btn-primary btn-circle btn-xs"><i class="fa fa-pencil"></i></button>');

			$btn .= "&nbsp;&nbsp;"."<button title='Delete' onclick='return deleteRecord(".$Data->id.")' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i></button>";

			$banner_image = "<img src='".base_url('uploads/banners/'.$Data->banner_image)."' alt='image' class='img-responsive' style='width:50px;height:40px; '>";


			if($Data->status=="Active")
			{
			 $status = '<strong style="color:red" title="Active" onclick="return status_change('.$Data->id.')"><span class="btn btn-success btn-xs" data-skin="skin-red"> <i class="fa fa-unlock"></i> </span> </strong>';                
			}
			else if($Data->status=="Inactive")
			{
			  $status = '<strong style="color:red" title="Inactive" onclick="return status_change('.$Data->id.')"><span class="btn btn-danger btn-xs" data-skin="skin-red"> <i class="fa fa-lock"></i> </span> </strong>';    
			}

			$nestedData = array();
			$nestedData[] = $banner_image;
			$nestedData[] = ucwords($Data->banner_title);
			$nestedData[] = $status;
			$nestedData[] = $btn;
			$data[] = $nestedData;
			$star = array();
      	}

        
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->DemoModel->count_all(),
			"recordsFiltered" => $this->DemoModel->count_filtered(),
			"data" => $data,
		); 
      	//output to json format
      	echo json_encode($output);
  	}
}
