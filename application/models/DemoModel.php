<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class DemoModel extends CI_Model
{ 
    public $table = 'banners';
    var $column_order = array(null,'banners.banner_title','banners.banner_image','banners.status',null,null); //set column field database for datatable orderable
    var $column_search = array('banners.banner_title','banners.banner_image','banners.status'); //set column field database for datatable searchable 
    var $order = array('id' => 'desc'); 
    function __construct()
    {
        parent::__construct();
    }
    
    private function _get_datatables_query()
    {
       
        $this->db->select('banners.*');
        $this->db->from('banners');
       
        $i = 0;
        
        if($_POST['search']['value']) // if datatable send POST for search
        { 
            $explode_string = explode(' ', $_POST['search']['value']); 
            foreach ($explode_string as $show_string) 
            {   
                if($explode_string[0] == 'In' || $explode_string[0] == 'in' || $explode_string[0] == 'IN' || $explode_string[0] == 'Out'|| $explode_string[0] == 'OUT'|| $explode_string[0] == 'out')
                {  
                    $cond  = " ";
                    $cond.=" ( banners.status LIKE '".$explode_string[0]."%' ) ";
                   
                } else {

                    $cond  = " ";
                    $cond.="( banners.banner_title  LIKE '%".trim($show_string)."%' ";
                   
               
                } 
                 $this->db->where($cond);
            }
        }
         $i++;
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }


}